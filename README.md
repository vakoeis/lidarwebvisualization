# lidarViz Lidar web visualizer
Visualizes our lidar point cloud data and clusters and it's pretty.

## Setup

1. install node and npm (use nvm, the node version manager)
2. `npm install`
3. `npm install parcel -g` (make sure to get v1.12.2 or later)

## Run

0. Start the dummy point cloud data generator and the node process which sends data to the visualization
1. `parcel index.html`
2. …
3. profit

## Dev

Please use ESLint with the provided config .eslintrc: `eslint js/script.js `
