import {
	IEEE754, mapInterval
} from "./Tools";

// formats the data from the scanner
export function processPointCloud(data) {
	var pointArray = [];
	for (let datapoint = 0; datapoint < data.length; datapoint += 16) {
		const vars = [];
		for (let varNum = 0; varNum < 4; varNum++) {
			vars[varNum] = 0;
			for (let i = 0; i < 4; i++) {
				// little endian
				const byte = data[datapoint + varNum * 4 + i];
				vars[varNum] += byte << 8 * i;
			}
		}


		var point = {
			x: IEEE754(vars[0]),
			y: IEEE754(vars[1]),
			intensity: IEEE754(vars[3])
		};
		pointArray.push(point);
	}

	return pointArray;
}

export function heatmap(val) {
	// console.info("int:", val);
	return `hsla(${Math.floor(mapInterval(val, 0, 17500, 340, 120))}, 100%, 50%, ${mapInterval(val*.8, 0, 15000, 1, 0.1).toFixed(4)})`;
	// return `rgba(0,0,0,${Math.floor(mapInterval(val, 10000, 15000, 1, 0)})`;
}
