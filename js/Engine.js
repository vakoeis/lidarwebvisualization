import {
	sleep
} from "./Tools";


export class Vector {

	constructor(...coords) {
		this.coords = coords;
	}

	get x() {
		return this.coords[0];
	}
	get y() {
		return this.coords[1];
	}
	get z() {
		return this.coords[2];
	}

	set x(val) {
		this.coords[0] = val;
	}
	set y(val) {
		this.coords[1] = val;
	}
	set z(val) {
		this.coords[2] = val;
	}

	get size() {
		return this.coords.length;
	}

	scale(s) {
		return new Vector(...this.coords.map(val => val*s));
	}

	add(v) {
		if (this.size !== v.size) throw new Error("incompatible vector lengths")
		return new Vector(...this.coords.map(
			(coord, i) => coord + v.coords[i]
		));
	}

	equals(v) {
		return this.coords.every((coord, i) => v.coords[i]);
	}

}



export class Matrix {

	constructor(...columnVectors) {
		this.columnVectors = columnVectors;
	}

	// get x() {
	// 	return this.columnVectors[0];
	// }
	// get y() {
	// 	return this.columnVectors[1];
	// }
	// get z() {
	// 	return this.columnVectors[2];
	// }

	// set x(val) {
	// 	this.columnVectors[0] = val;
	// }
	// set y(val) {
	// 	this.columnVectors[1] = val;
	// }
	// set z(val) {
	// 	this.columnVectors[2] = val;
	// }

	get size() {
		return this.columnVectors.length;
	}

	/** multiply from left with other matrix */
	leftMultiplyM(rightMatrix) {
		const newColumnVectors = rightMatrix.columnVectors.map(rightColumnVector => {
			const scaledLeftColumnVectors =
				rightColumnVector.coords.map((coord, coordI) =>
					this.columnVectors[coordI].scale(coord)
				)
			return scaledLeftColumnVectors
				.reduce(
					(a, b) => a.add(b)
				);
		});
		return new Matrix(...newColumnVectors);
	}
	/** multiply from left with vector */
	leftMultiplyV(vector) {
		const scaledLeftColumnVectors =
			vector.coords.map((coord, coordI) =>
				this.columnVectors[coordI].scale(coord)
			)
		return scaledLeftColumnVectors
			.reduce(
				(a, b) => a.add(b)
			);
	}

	add(v) {
		if (this.size !== v.size) throw new Error("incompatible vector lengths")
		this.columnVectors = this.columnVectors.map(
			(coord, i) => coord + v.columnVectors[i]
		);
	}

	equals(M) {
		if (!(M instanceof Matrix)) throw new Error("``M'' is no Matrix")
		return this.columnVectors.every((colv, i) => colv.equals(M.columnVectors[i]));
	}

}
function getIdentityMatrix2D() {
	return new Matrix(new Vector(1, 0), new Vector(0, 1));
}

export class Drawable {

	draw(c, ctx, state) {

	}

	async live() {

	}

}

export class Box extends Drawable {

	constructor(pos, dim, spd, color = "black") {
		super();
		this.color = color;
		this.pos = pos; // position
		this.dim = dim; // dimensions
		this.spd = spd; // speed
	}

	move() {
		this.pos = this.pos.add(this.spd);
	}
	async live() {
		this.move();
	}

	collidesWith(collider) {
		if (!(collider instanceof Box))
			throw new Error("``collider'' is not a box")
		// if (this.pos.x > 50 && this.pos.y < -110 && this.pos.y > -130) debugger;
		return (
			this.pos.x < collider.pos.x + collider.dim.x &&
			this.pos.x + this.dim.x > collider.pos.x &&
			this.pos.y < collider.pos.y + collider.dim.y &&
			this.pos.y + this.dim.y > collider.pos.y
		);
	}
	collidesWithFuture(collider) {
		if (!(collider instanceof Box))
			throw new Error("``collider'' is not a box")

		const nextPos = this.pos.add(this.spd);

		// if (this.pos.x > 50 && this.pos.y < -110 && this.pos.y > -130) debugger;
		return (
			nextPos.x < collider.pos.x + collider.dim.x &&
			nextPos.x + this.dim.x > collider.pos.x &&
			nextPos.y < collider.pos.y + collider.dim.y &&
			nextPos.y + this.dim.y > collider.pos.y
		);
	}
}

window.Box = Box;

export class Rect extends Box {

	// well draws tiny rectangles ¯\_(ツ)_/¯

	constructor(pos, dim, spd, color = "black") {
		super(pos, dim, spd, color)
	}

	draw(c, ctx, state) {
		// console.log(color)
		ctx.fillStyle = this.color;
		ctx.beginPath();
		ctx.fillRect(
			this.pos.x, this.pos.y,
			this.dim.x, this.dim.y
		);
		// ctx.arc(x, y, 2, 0, 2 * Math.PI);
		ctx.stroke();
		ctx.closePath();
	}

}


export class CollisionBox extends Box {

	constructor(pos, dim, spd, color = "black") {
		super(pos, dim, spd, color);
		this.collisionPartners = [];
	}

	draw(c, ctx, state) {
		// console.log(color)
		ctx.fillStyle = this.color;
		ctx.beginPath();
		ctx.fillRect(
			this.pos.x, this.pos.y,
			this.dim.x, this.dim.y
		);
		// ctx.arc(x, y, 2, 0, 2 * Math.PI);
		ctx.stroke();
		ctx.closePath();
	}

	getSpeedChangeMatrix(collider) { // returns matrix to multiply with spd
		if (!(collider instanceof Box)) throw new Error("``collider'' is not a box")

		const speedChange = new Matrix(
			new Vector(1, 0), new Vector(0, 1)
		);

		const nextPos = this.pos.add(this.spd);

		if (nextPos.x < collider.pos.x + collider.dim.x &&
			nextPos.x + this.dim.x > collider.pos.x &&
			nextPos.y < collider.pos.y + collider.dim.y &&
			nextPos.y + this.dim.y > collider.pos.y
		) {
			// this is shit code.

			// console.log("soidhfpaoudshf")
			if (
				this.pos.x + this.dim.x <= collider.pos.x ||
				this.pos.x >= collider.pos.x + collider.dim.x
			) {
				// console.log("was left or right")
				speedChange.columnVectors[0].x = -1; // x is inverted
			} else if (
				this.pos.y + this.dim.y <= collider.pos.y ||
				this.pos.y >= collider.pos.y + collider.dim.y
			) {
				// console.log("was above or below")
				// console.log("horizontal")
				speedChange.columnVectors[1].y = -1; // y is inverted
				// console.log(speedChange)
			}
		}
		return speedChange;
	}

}


export class Ball extends CollisionBox {

	constructor(pos, dim, spd, color = "black") {
		super(pos, dim, spd, color);

		this.pointsTop = 0;
		this.pointsBottom = 0;
		this.initialPos = this.pos;

		// const v = new Vector(1, 1);
		// console.log(v.scale(3));
		// const w = new Vector(0, 1);
		// console.log(w.scale(4));

		// const M = new Matrix(
		// 	new Vector(2, 0), new Vector(0, 1)
		// );
		// const N = new Matrix(
		// 	new Vector(3, 0), new Vector(0, 3)
		// );
		// console.log(M.leftMultiplyM(N))
		// console.log(M.leftMultiplyV(v))
	}

	async reset() {
		this.pointsTop = this.pointsBottom = 0;
		this.pos = this.initialPos;
		this.spd = new Vector(5, 5);
		await sleep(1000);
	}

	point() {
	}

	async live() {
		if (this.collidesWithFuture(this.topPointCollider)) {
			if (this.pointsTop++ === 7) await this.reset();
			this.point();
			console.log("Top scored!", this.pointsTop)
		} else if (this.collidesWithFuture(this.bottomPointCollider)) {
			if (this.pointsBottom++ === 7) await this.reset();
			this.point();
			console.log("Bottom scored!", this.pointsBottom)
		}
		// if (this.resetCollisionPartners.some(partner => this.collidesWithFuture(partner))) {

		// }

		let spdChangeMatrix = getIdentityMatrix2D();

		this.collisionPartners.forEach(partner => {
			// debugger;
			spdChangeMatrix =
				this.getSpeedChangeMatrix(partner)
				.leftMultiplyM(spdChangeMatrix)
		});

		if (this.collisionPartners.some(partner => this.collidesWithFuture(partner))) {
			// leide
			// console.log("aua", this.$auas)
			// const $aua = this.$auas[Math.floor(this.$auas.length * Math.random())];
			// $aua.currentTime = 0;
			// $aua.play();
			console.log("aua")
		}

		this.spd = spdChangeMatrix.leftMultiplyV(this.spd);

		// this.spd = this.spd.scale(1.01);
		this.move();
	}

	draw(c, ctx, state) {

		ctx.font = "16px sans-serif, Comic Sans MS";

		// ctx.fillText(this.pointsTop + " Punkt" + (this.pointsTop !== 1 ? "e" : ""), -50, 100);
		// ctx.fillText(this.pointsBottom + " Punkt" + (this.pointsBottom !== 1 ? "e" : ""), -50, -100);

		// console.log(color)
		ctx.fillStyle = this.color;
		ctx.beginPath();
		ctx.ellipse(
			this.pos.x + this.dim.x/2, this.pos.y + this.dim.y/2,
			this.dim.x/2, this.dim.y/2,
			2 * Math.PI,
			0, 2 * Math.PI
		);
		// ctx.fillRect(
		// 	this.pos.x, this.pos.y,
		// 	this.dim.x, this.dim.y
		// );
		// ctx.stroke();
		ctx.fill();
		ctx.closePath();


	}
}


export class StripedPattern {

	// here you can create beautiful striped patterns;

	constructor(color1, color2, density, rotation) {
		// striped Canvas
		var stripedCanvas = document.createElement("canvas");
		var drawingContext = stripedCanvas.getContext("2d");
		stripedCanvas.width = 300;
		stripedCanvas.height = 300;

		var numberOfStripes = density;
		var thickness = stripedCanvas.width / numberOfStripes;
		for (var i = 0; i < numberOfStripes * 2; i++) {
			drawingContext.beginPath();
			drawingContext.strokeStyle = i % 2 ? color1 : color2;
			drawingContext.lineWidth = thickness;
			drawingContext.lineCap = "square";

			// diagonal stripes or vertical
			if (rotation === 1) {
				drawingContext.moveTo(i*thickness + thickness/2 - stripedCanvas.height, 0);
				drawingContext.lineTo(0 + i*thickness+thickness/2, stripedCanvas.height);
			} else {
				drawingContext.moveTo(i * thickness, 0);
				drawingContext.lineTo(i * thickness, stripedCanvas.width);
			}
			drawingContext.stroke();
		}

		this.pattern = drawingContext.createPattern(stripedCanvas, "repeat");
	}

}

export class FOV extends Drawable {

	constructor(bgPattern, strokeColor) {
		super();
		this.bgPattern = bgPattern;
		this.strokeColor = strokeColor;
	}

	draw(c, ctx, state) {
		ctx.beginPath();
		ctx.fillStyle = "white";
		ctx.strokeStyle = this.strokeColor;


		const nearestBorderDist = Math.min(
			c.width/2 * 1/state.scl,
			c.height/2 * 1/state.scl
		);

		const topX = -nearestBorderDist;
		const topY = -nearestBorderDist;

		const bottomX = -nearestBorderDist;
		const bottomY = nearestBorderDist;

		ctx.moveTo(topX, topY);
		ctx.lineTo(0, 0);
		ctx.lineTo(bottomX, bottomY);
		ctx.lineWidth = 2;
		ctx.stroke();

		ctx.lineTo(-c.width/2 * 1/state.scl, c.height/2 * 1/state.scl); // bottom left corner
		ctx.lineTo(-c.width/2 * 1/state.scl, -c.height/2 * 1/state.scl); // top left corner
		ctx.lineTo(topX, topY);

		ctx.fillStyle = this.bgPattern;
		ctx.fill();

		ctx.closePath();
	}

	async live() {}

}

export class Ellipse extends Box {

	constructor(pos, dim, spd, color = "black") {
		super(pos, dim, spd, color)
	}

	draw(c, ctx, state) {
		ctx.fillStyle = this.color;
		ctx.beginPath();
		ctx.ellipse(
			this.pos.x + this.dim.x/2, this.pos.y + this.dim.y/2,
			this.dim.x/2, this.dim.y/2,
			2 * Math.PI,
			0, 2 * Math.PI
		);
		// ctx.stroke();
		ctx.fill();
		ctx.closePath();
	}

}

export class Universe { // TODO extends Box

	constructor(things = []) {
		this.things = things
	}

	addThing(...things) { this.things.push(...things) };

	draw(c, ctx, state) {
		this.things.forEach(thing => thing.draw(c, ctx, state));
	}

	async live() {
		this.things.forEach(async thing => thing.live());
	}

}


export class Engine {

	constructor(universe, c, ctx, state) {
		this.universe = universe;
		this.c = c;
		this.ctx = ctx;
		this.state = state;
	}

	setupCanvas() {
		this.c.height = this.c.offsetHeight;
		this.c.width = this.c.offsetWidth;
		this.ctx.translate(this.c.width/2, this.c.height/2); // coordinates centered in the middle
		this.ctx.scale(1, -1);
		this.ctx.scale(this.state.scl, this.state.scl);
	}

	async updateCanvas() {
		return new Promise(res => window.requestAnimationFrame(async () => {
			this.ctx.clearRect(-this.c.width/2 * 1/this.state.scl, this.c.height/2 * 1/this.state.scl, this.c.width * 1/this.state.scl, -this.c.height * 1/this.state.scl); // idk about that -
			this.universe.draw(this.c, this.ctx, this.state);
			res();
		}))
	}

	async loop() {
		while (true) {
			// if (this.state.isPlaying)
			await this.universe.live();
			await this.updateCanvas()
			await sleep(this.state.frameDelay);
		}
	}

}

