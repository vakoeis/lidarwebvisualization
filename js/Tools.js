
export const arraysEqual = (a, b, isEqual = (a, b) => a === b) => {
	// if the other array is a falsy value, return
	if (!b) return false;

	// compare lengths - can save a lot of time
	if (a.length !== b.length)
		return false;

	for (var i = 0, l = a.length; i < l; i++) {
		// Check if we have nested arrays
		if (a[i] instanceof Array && b[i] instanceof Array) {
			// recurse into the nested arrays
			if (!a[i].arraysEqual(b[i]))
				return false;
		}
		else if (!isEqual(a[i], b[i])) {
			// Warning - two different object instances will never be equal: {x:20} != {x:20}
			return false;
		}
	}
	return true;
}

export const flatten = arr => [].concat(...arr);

// input with dynamic width
// https://codepen.io/w3core/pen/DjLml
export const resizable = (el, factor = 7.7) => {
	if (el instanceof NodeList || Array.isArray(el))
		return el.forEach(el => resizable(el, factor));

	var int = Number(factor);
	const resize = () => {
		el.style.width = ((el.value.length + 1) * int) + 26 + 'px'
	}

	['keydown', 'focus', 'blur', 'change'].forEach(e =>
		el.addEventListener(e, () => requestAnimationFrame(resize) /* keydown delay */ , false)
	);
	resize();
}

/**
 * @param {String} HTML representing any number of sibling elements
 * @return {NodeList}
 */
export const htmlToElement = html => {
	var template = document.createElement("template");
	template.innerHTML = html;
	return template.content.childElementCount === 1 ?
		template.content.firstElementChild : template.content;
}

/**
 * gives all combinations of values for some length of elements
 * @param {Integer} comblen combination length, how many elements are in a combi
 * @param {Array} values possibilities for values
 * @return {Array} of combinations
 */
export const getCombinations = (combLen, values) => {
	// TODO do this recursively?
	const digits = combLen,
		base = values.length;
	const combinations = new Array(Math.pow(base, digits)).fill(null);

	return combinations.map((_, combI) =>
		// combI "combinationIndex" represents a number in base `values.length`
		// where each digit is an index for a possibility
		new Array(combLen).fill(null).map((_, posI) => {
			const digit =
				Math.floor(combI / Math.pow(base, posI)) // cut off right part
				%
				Math.pow(base, digits - posI); // cut off left part
			return values[digit % base]; // TODO fix bug later
		})
	)
}

export function shuffleArray(array) {
	var currentIndex = array.length, temporaryValue, randomIndex;

	// While there remain elements to shuffle...
	while (0 !== currentIndex) {

		// Pick a remaining element...
		randomIndex = Math.floor(Math.random() * currentIndex);
		currentIndex -= 1;

		// And swap it with the current element.
		temporaryValue = array[currentIndex];
		array[currentIndex] = array[randomIndex];
		array[randomIndex] = temporaryValue;
	}

	return array;
}

// https://stackoverflow.com/a/36566052

function editDistance(s1, s2) {
	s1 = s1.toLowerCase();
	s2 = s2.toLowerCase();

	var costs = new Array();
	for (var i = 0; i <= s1.length; i++) {
		var lastValue = i;
		for (var j = 0; j <= s2.length; j++) {
			if (i == 0)
				costs[j] = j;
			else {
				if (j > 0) {
					var newValue = costs[j - 1];
					if (s1.charAt(i - 1) != s2.charAt(j - 1))
						newValue = Math.min(Math.min(newValue, lastValue),
							costs[j]) + 1;
					costs[j - 1] = lastValue;
					lastValue = newValue;
				}
			}
		}
		if (i > 0)
			costs[s2.length] = lastValue;
	}
	return costs[s2.length];
}

export function stringSimilarity(s1, s2) {
	var longer = s1;
	var shorter = s2;
	if (s1.length < s2.length) {
		longer = s2;
		shorter = s1;
	}
	var longerLength = longer.length;
	if (longerLength == 0) {
		return 1.0;
	}
	return (longerLength - editDistance(longer, shorter)) / parseFloat(longerLength);
}


export async function sleep(time) {
	return new Promise(res => setTimeout(res, time))
}

// Returns a function, that, as long as it continues to be invoked, will not
// be triggered. The function will be called after it stops being called for
// N milliseconds. If `immediate` is passed, trigger the function on the
// leading edge, instead of the trailing.
export function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
};


// converts an 32bit unsigned integer to a 32 bit float;
export function IEEE754(num) {
	// von wikipedia
	const sign = (num & 0b10000000000000000000000000000000) < 0 ? 1 : 0;
	// if the value overflows, we know there was a one at the first position
	const exponent = (num & 0b01111111100000000000000000000000) >> 23;
	const mantissa = (num & 0b00000000011111111111111111111111);
	// console.log(sign, exponent, mantissa);
	const actualExponent = exponent - 127;
	const modMantissa = ((mantissa + 0b100000000000000000000000) / 2 ** 23) * 2 ** actualExponent;
	return modMantissa * ((-1) ** sign);
}

export function mapInterval(num, from, to, from2, to2) {
	return (to - num) / (to - from) * (to2 - from2) + from2
}
