import QRCode from 'qrcode';

export default function generateQRCode() {

	QRCode.toCanvas(
		document.querySelector("#qrcode"),
		location.origin + location.pathname, {// not include hash
			// version: 3,
			color: {
				dark: "7722aa99"
			},
			scale: 32
		}, error => {
			if (error) console.error(error)
		})

	document.querySelector(".qrcodelink").onclick = function() {
		if (location.hash) setTimeout(() => {
			location.hash = "";
		}, 0);
	}

}
