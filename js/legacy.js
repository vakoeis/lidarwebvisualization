
const c = document.querySelector('canvas');
const ctx = c.getContext("2d");
// resolution could now be adjusted dynamically to window size
const height = 1050,
	width = 1884;

const dataSelector = document.querySelector('#data');
const frameDelaySlider = document.querySelector('.frameDelay');
const slider = document.querySelector('.slider');
const scl = document.querySelector('.scl');
const dangerZone = document.querySelector('.dangerZone');
const adjacencyRadius = document.querySelector('.adjacencyRadius');
const pointThresholdSlider = document.querySelector('.pointThreshold');

var stopOnCollision = false;
var play = true;
// const scl = .5;
// .5 = 1cm Bildschirm : 10cm Wirklichkeit

window.onkeypress = function(event) {
	if (event.keyCode == 32) play = !play;
};


async function wait(time) {
	return new Promise(res => setTimeout(res, time));
}

// well draws tiny rectangles ¯\_(ツ)_/¯
function drawRectAt(x, y, color) {
	// console.log(color)
	ctx.fillStyle = color;
	ctx.beginPath();
	ctx.fillRect(x, y, 4, 4);
	// ctx.arc(x, y, 2, 0, 2 * Math.PI);
	ctx.stroke();
	ctx.closePath();
}

// here you can create beautiful striped patterns;
function createStripedPattern(color1, color2, density, rotation) {
	// striped Canvas
	var stripedCanvas = document.createElement('canvas');
	var drawingContext = stripedCanvas.getContext("2d");
	stripedCanvas.width = 300;
	stripedCanvas.height = 300;

	var numberOfStripes = density;
	var thickness = stripedCanvas.width / numberOfStripes;
	for (var i = 0; i < numberOfStripes * 2; i++) {
		drawingContext.beginPath();
		drawingContext.strokeStyle = i % 2 ? color1 : color2;
		drawingContext.lineWidth = thickness;
		drawingContext.lineCap = 'square';

		// diagonal stripes or vertical
		if(rotation == 1){
			drawingContext.moveTo(i*thickness + thickness/2 - stripedCanvas.height,0);
			drawingContext.lineTo(0 + i*thickness+thickness/2,stripedCanvas.height);
		} else {
			drawingContext.moveTo(i * thickness, 0);
			drawingContext.lineTo(i * thickness, stripedCanvas.width);
		}
		drawingContext.stroke();
	}
	// pattern
	var stripedPattern = ctx.createPattern(stripedCanvas, "repeat");
	return stripedPattern;
}


// function for drawing all points of a dataset adjusted to the window size
function drawPoints(dataset) {
	for (let point of dataset) {
		const adjX = point.x * ((height + width ) / 2)* scl.value + width / 2,
			adjY = point.y * ((height + width ) / 2) * scl.value + height / 2;
		drawRectAt(adjX, adjY, `hsla(${Math.floor(mapInterval(point.intensity, 10000, 15000, 200, 320))}, 100%, 50%, 1)`);
	}
}


// beautiful striped patterns
const stripedPatternCircle = createStripedPattern("#555", "#333", 14, 0);
const stripedPatternFOV = createStripedPattern("#ccc", "#f0f0f0", 24, 1);


// draws Helpful visualizations e.g. the FOV
function drawBackground() {

	// big circle representing emergency zone
	ctx.beginPath();
	ctx.strokeStyle = '#FF0000';
	ctx.arc(width / 2, height / 2, dangerZone.value * ((height + width ) / 2) * scl.value, 0, 2 * Math.PI);
	ctx.stroke();
	ctx.closePath();

	// small circle representing sickscanner
	ctx.beginPath();
	ctx.strokeStyle = '#000';
	ctx.arc(width / 2, height / 2, 15*scl.value, 0, 2 * Math.PI);
	ctx.fillStyle = stripedPatternCircle;
	ctx.fill();
	ctx.stroke();
	ctx.closePath();

	// field of vision
	ctx.beginPath();
	ctx.fillStyle = "white";
	ctx.strokeStyle = "black";
	var m =width/2 + height/2;
	var x1,x2,y1,y2;
	if(m<= height){
		x1 = 0;
		y1 = m;
		x2 = 0;
		y2 = height-m;
	}else{
		x1 = m-height;
		y1 = height;
		x2 = m-height;
		y2 = 0;
	}
	ctx.moveTo(x1,y1);
	ctx.lineTo(width/2, height/2);
	ctx.lineTo(x2,y2);
	ctx.lineTo(0,0);
	ctx.lineTo(0,height);
	ctx.lineTo(x1,y1);
	ctx.fillStyle = stripedPatternFOV;
	ctx.fill();
	ctx.stroke();
	ctx.closePath();
}


// formats the data from the scanner
function processData(data) {
	var dataArray = [];
	for (let dataset = 0; dataset < data.length; dataset++) {
		var pointArray = [];
		for (let datapoint = 0; datapoint < data[dataset].length; datapoint += 16) {
			const vars = [];
			for (let varNum = 0; varNum < 4; varNum++) {
				vars[varNum] = 0;
				for (let i = 0; i < 4; i++) {
					// little endian
					const byte = data[dataset][datapoint + varNum * 4 + i];
					vars[varNum] += byte << 8 * i;
				}
			}


			var point = {
				x: IEEE754(vars[0]),
				y: IEEE754(vars[1]),
				intensity: IEEE754(vars[3])
			};
			pointArray.push(point);
		}
		dataArray.push(pointArray);
	}

	return dataArray;
}

// every checkRate point gets checked for his circle of doom
const checkRate = 3;
// how many adjacent pixels are checked
const adjAmount = 20;

// algorithm for detecting possible collisions
function detectEmergencies(dataset) {
	// circle of doom size// adjacencyRadius
	var doomSize = adjacencyRadius.value;
	// How many points must lie in the circle of doom for an emergency brake to occur
	var pointThreshold = pointThresholdSlider.value;

	for (let datapoint = 0; datapoint < dataset.length; datapoint += checkRate) {
		const point = dataset[datapoint];
		if(Math.sqrt(point.x**2 + point.y**2)  < dangerZone.value){
			const adjX = point.x * ((height + width ) / 2)* scl.value + width / 2,
				adjY = point.y * ((height + width ) / 2) * scl.value + height / 2;
			drawRectAt(adjX, adjY, '#000');
			var containedPoints = 0;
			for(let adjacentDataPoint = Math.max(datapoint-adjAmount, 0); adjacentDataPoint < Math.min(datapoint+adjAmount, dataset.length); adjacentDataPoint++){
				const adjacentPoint = dataset[adjacentDataPoint];
				if(Math.abs(point.x - adjacentPoint.x) + Math.abs(point.y - adjacentPoint.y) < doomSize){
					containedPoints++;
				}
			}
			if (containedPoints >= pointThreshold) {
				ctx.beginPath();
				ctx.arc(adjX, adjY, doomSize * ((height + width ) / 2) * scl.value, 0, 2 * Math.PI);
				ctx.strokeStyle = '#000';
				ctx.stroke();
				ctx.closePath();
				if(stopOnCollision){
					play = false;
				}
			}

		}
	}

}




function mapInterval(num, from, to, from2, to2) {
	return (to - num) / (to - from) * (to2 - from2) + from2
}

// converts an 32bit unsigned integer to a 32 bit float;
function IEEE754(num) {
	// von wikipedia
	const sign = (num & 0b10000000000000000000000000000000) < 0 ? 1 : 0;
	// if the value overflows, we know there was a one at the first position
	const exponent = (num & 0b01111111100000000000000000000000) >> 23;
	const mantissa = (num & 0b00000000011111111111111111111111);
	// console.log(sign, exponent, mantissa);
	const actualExponent = exponent - 127;
	const modMantissa = ((mantissa + 0b100000000000000000000000) / 2 ** 23) * 2 ** actualExponent;
	return modMantissa * ((-1) ** sign);
}


var datas = {
	data: processData(window.data),
	data2: processData(window.data2),
	daterrrg: processData(window.daterrrg),
	daterrrgCut: processData(window.daterrrgCut),
	testEnvironment: processData(window.testEnvironment),
	testBallBlue: processData(window.testBallBlue),
	testBallRed: processData(window.testBallRed),
	testBallMirror: processData(window.testBallMirror),
	testBallMixed: processData(window.testBallMixed),
	testBallRollingBlue: processData(window.testBallRollingBlue),
	testBallRollingBlue2: processData(window.testBallRollingBlue2),
	testBallRollingMirror: processData(window.testBallRollingMirror),
	testBallRollingMirror2: processData(window.testBallRollingMirror2),
	testBallRollingRed: processData(window.testBallRollingRed),
	testBallRollingRed2: processData(window.testBallRollingRed2),
	testBallRollingTowardsScannerBlueRedMirror: processData(window.testBallRollingTowardsScannerBlueRedMirror),
	testBallRollingTowardsScannerBlueRedMirror2: processData(testBallRollingTowardsScannerBlueRedMirror2)
}
const getData = () => datas[dataSelector.value];


async function main() {
	// hide loading animation
	const loading = document.getElementById('loading');
	loading.style.visibility = "hidden";

	// mainloop
	while (true) {
		// frame iteration
		for (slider.value = 0; slider.value < getData().length; slider.value++) {
			slider.max = getData().length;
			ctx.clearRect(0, 0, width, height);
			const dataset = getData()[slider.value];

			drawBackground();
			drawPoints(dataset);
			detectEmergencies(dataset);

			await wait(frameDelaySlider.value);
			while (!play) {
				await wait(5);
			}
		}
		slider.value = 0;
	}

}

main();
