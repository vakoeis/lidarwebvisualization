import Peer from "simple-peer"; // WebRTC
import io from "socket.io-client";

import {
	lidarIP,

	connectHandler,
	disconnectHandler,
	dataHandler,
	connectionTimeoutHandler,
	errorHandler
} from "./script.js"


function jsonSummary(json) {
	return getSummary(JSON.stringify(json));
}

/** returns first and last chars of a string */
function getSummary(s) {
	if (s.length <= 100) return s;
	return s.substring(0, 50) + "…" + s.substring(s.length - 50, s.length);
}


if (!Peer.WEBRTC_SUPPORT) {
	throw new Error("no webrtc support :(")
}


let serverSocket;

export function close() {
	serverSocket.close(); // also closes peer connection
}

export function connect() {
	serverSocket = io(lidarIP);

	serverSocket.on("connect", () => {
		const id = serverSocket.id; // 'G5p5...'
		console.log("("+id+")", "connected via websocket");

		const clientPeer = new Peer({
			objectMode: true,
			initiator: true,
			trickle: false,
			iceTransportPolicy: "relay",
			reconnectTimer: 3000,
			config: {
				"iceServers": [
					{
						url: "stun:eu-turn4.xirsys.com",
						urls: "stun:eu-turn4.xirsys.com",
						"username": "UlBvoMNYBgIumfXF34CGim4bUMO7kZdaOxfM3pk1VrV7A_JP2s47tyVNyPYDGlYOAAAAAFzjFqhpanVzdHdhbnRhc2VydmVy",
						"credential": "0863149a-7b43-11e9-9435-6adcafebbb45"
					}, {
						url: "turn:eu-turn4.xirsys.com:80?transport=udp",
						urls: "turn:eu-turn4.xirsys.com:80?transport=udp",
						"username": "UlBvoMNYBgIumfXF34CGim4bUMO7kZdaOxfM3pk1VrV7A_JP2s47tyVNyPYDGlYOAAAAAFzjFqhpanVzdHdhbnRhc2VydmVy",
						"credential": "0863149a-7b43-11e9-9435-6adcafebbb45"
					}, {
						url: "turn:eu-turn4.xirsys.com:3478?transport=udp",
						urls: "turn:eu-turn4.xirsys.com:3478?transport=udp",
						"username": "UlBvoMNYBgIumfXF34CGim4bUMO7kZdaOxfM3pk1VrV7A_JP2s47tyVNyPYDGlYOAAAAAFzjFqhpanVzdHdhbnRhc2VydmVy",
						"credential": "0863149a-7b43-11e9-9435-6adcafebbb45"
					}, {
						url: "turn:eu-turn4.xirsys.com:80?transport=tcp",
						urls: "turn:eu-turn4.xirsys.com:80?transport=tcp",
						"username": "UlBvoMNYBgIumfXF34CGim4bUMO7kZdaOxfM3pk1VrV7A_JP2s47tyVNyPYDGlYOAAAAAFzjFqhpanVzdHdhbnRhc2VydmVy",
						"credential": "0863149a-7b43-11e9-9435-6adcafebbb45"
					}, {
						url: "turn:eu-turn4.xirsys.com:3478?transport=tcp",
						urls: "turn:eu-turn4.xirsys.com:3478?transport=tcp",
						"username": "UlBvoMNYBgIumfXF34CGim4bUMO7kZdaOxfM3pk1VrV7A_JP2s47tyVNyPYDGlYOAAAAAFzjFqhpanVzdHdhbnRhc2VydmVy",
						"credential": "0863149a-7b43-11e9-9435-6adcafebbb45"
					}, {
						url: "turns:eu-turn4.xirsys.com:443?transport=tcp",
						urls: "turns:eu-turn4.xirsys.com:443?transport=tcp",
						"username": "UlBvoMNYBgIumfXF34CGim4bUMO7kZdaOxfM3pk1VrV7A_JP2s47tyVNyPYDGlYOAAAAAFzjFqhpanVzdHdhbnRhc2VydmVy",
						"credential": "0863149a-7b43-11e9-9435-6adcafebbb45"
					}, {
						url: "turns:eu-turn4.xirsys.com:5349?transport=tcp",
						urls: "turns:eu-turn4.xirsys.com:5349?transport=tcp",
						"username": "UlBvoMNYBgIumfXF34CGim4bUMO7kZdaOxfM3pk1VrV7A_JP2s47tyVNyPYDGlYOAAAAAFzjFqhpanVzdHdhbnRhc2VydmVy",
						"credential": "0863149a-7b43-11e9-9435-6adcafebbb45"
					}
				]
			}
		})

		// when clientPeer has signaling data, send it to serverSocket
		clientPeer.on("signal", data => {
			console.log("("+id+")", "sending signaling data", data);
			serverSocket.emit("signaling_message", data); // -> serverSocket.signal(data)
		})


		// getting signaling messages from the serverSocket
		serverSocket.on("signaling_message", data => {
			console.log("("+id+")", "received signaling data", data);

			clientPeer.signal(data);
		});



		clientPeer.on("error", err => {
			console.error("("+id+") peer to peer error:", err);
			errorHandler(err);
		})

		clientPeer.on("connect", () => {
			connectHandler();
			console.log("("+id+")", "Connected peer to peer to server!")
			// wait for "connect" event before using the data channel

		})

		clientPeer.on("data", data => {
			// got a data channel message
			// console.log("("+id+")", "got a message from serverSocket: " + data);
			dataHandler(data);
		})

		clientPeer.on("close", data => {
			// got a data channel message
			console.log("("+id+")", "disconnected peer to peer");
		})

		serverSocket.on("disconnect", () => {
			disconnectHandler();
			clientPeer.destroy()
		});

	});
}
