parcel build index.html --out-dir lidarviz/ --public-url /lidarviz/ --detailed-report
tar -cf lidarviz.tar lidarviz
echo
echo now uploading
scp lidarviz.tar vogt:/home/m/apps/portfolio/public/
echo
# ssh vogt "rm -rf /home/m/apps/portfolio/public/dist/"
ssh vogt "rm -rf /home/m/apps/portfolio/public/lidarviz/"
ssh vogt "cd /home/m/apps/portfolio/public/ && tar -xmf /home/m/apps/portfolio/public/lidarviz.tar"
# ssh vogt "cd /home/m/apps/portfolio/public/ && mv ./dist/ ./lidarviz/"
echo done!
